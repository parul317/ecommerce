<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'frontcontroller@index');
Route::match(['get','post'],'/product_details/{id}','frontcontroller@product_details');

Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');

Route::get('dashboard', 'admincontroller@index');

Route::get('add_banner', 'admincontroller@add_banner');
Route::post('store_banner', 'admincontroller@store_banner');
Route::get('view_banner', 'admincontroller@view_banner');
Route::match(['get','post'],'/edit_banner/{id}','admincontroller@editbanner');
Route::match(['get','post'],'/delete_banner/{id}','admincontroller@deletebanner');


//category controller
Route::match(['get','post'],'/add_category','categorycontroller@addcategory');
Route::match(['get','post'],'/view_category','categorycontroller@viewcategory');
Route::match(['get','post'],'/edit_category/{id}','categorycontroller@editcategory');
Route::match(['get','post'],'/delete_category/{id}','categorycontroller@deletecategory');
//delete multiple category
Route::post('/delete_multiple_category','categorycontroller@delete_multiple_category');

//ajax route
Route::get('/category_status/{id}','categorycontroller@category_status');

// product controller
Route::match(['get','post'],'/add_product','productcontroller@addproduct');
Route::match(['get','post'],'/view_product','productcontroller@viewproduct');
Route::match(['get','post'],'/edit_product/{id}','productcontroller@editproduct');
Route::match(['get','post'],'/delete_product/{id}','productcontroller@deleteproduct');
Route::post('/delete_multiple_product','productcontroller@delete_multiple_product');
Route::match(['get', 'post'],'/add_images/{id}', 'productcontroller@add_images');
Route::match(['get','post'],'/delete_image/{id}','productcontroller@delete_image');
Route::post('/delete_multiple_images','productcontroller@delete_multiple_image');

Route::get('/product_status/{id}','productcontroller@product_status');


Route::match(['get','post'],'/addCart','productcontroller@addtoCart');
Route::match(['get','post'],'/cart','productcontroller@cart');

Route::get('/cart/delete/{id}','productcontroller@deleCart');

Route::get('/cart/update-quantity/{id}/{quantity}', 'productcontroller@updatecartquantity');



Route::match(['get','post'],'/add_coupon','couponscontroller@addcoupon');
Route::match(['get','post'],'/view_coupon','couponscontroller@viewcoupon');
Route::match(['get','post'],'/edit_coupon/{id}','couponscontroller@editcoupon');
Route::match(['get','post'],'/delete_coupon/{id}','couponscontroller@deletecoupon');
Route::post('/delete_multiple_coupon','couponscontroller@delete_multiple_coupon');

//ajax route
Route::get('/coupon_status/{id}','couponscontroller@coupon_status');

Route::post('/cart/apply-coupon','productcontroller@applyCoupon');


//Route for login-register
Route::get('/login-register','UserController@userLoginRegister');
//Route for add users registration
Route::post('/user-register','UserController@register');
//Route for user logout
Route::get('/user-logout','UserController@logout');
//Route for login-user
Route::post('/user-login','UserController@login');

//Route for middleware after front login
//Route::group(['middleware'=>['frontlogin']],function(){
	
	//Route for user account
	Route::match(['get','post'],'/account','UserController@account');
	Route::match(['get','post'],'/change-password','UserController@changePassword');
	Route::match(['get','post'],'/change-address','UserController@changeAddress');
	Route::match(['get','post'],'/checkout','ProductController@checkout');
	Route::match(['get','post'],'/order_review','ProductController@orderreview');

	Route::match(['get','post'],'/place-order','ProductController@placeOrder');
	Route::match(['get','post'], '/thanks', 'ProductController@thanks');
	Route::get('/orders','ProductController@userorders');
	Route::get('/orders/{id}','ProductController@userOrderDetails');
//});


Route::match(['get','post'], '/view_order', 'orderController@viewOrder');
Route::get('/admin/orders/{id}','orderController@viewOrderDetail');

Route::post('/paytm-callback', 'ProductController@paytmCallback');

Route::post('/pay', 'ProductController@pay');