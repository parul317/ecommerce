@extends('admin.layout.master')
@section('content')
  
  <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-users"></i>
               </div>
               <div class="header-title">
                  <h1>Edit Category</h1>
                  <small>Edit Category</small>
               </div>
            </section>
            @if(session('message'))

           <p class ="alert alert-success">
            {{session('message')}}
           </p>
            
            @endif
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="{{url('view_category')}}"> 
                              <i class="fa fa-list"></i>  category List </a>  
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" method="post" action="{{url('/edit_category/'.$data->id)}}" enctype="multipart/form-data">
                              @csrf
                              <div class="form-group">
                                 
                                 <label>Category Name</label>
                                 <input type="text" class="form-control" value="{{$data->name}}" name="name">
                              </div>

                              <div class="form-group">
                               <label>Parent Category</label>  
                               <select name="parent_id" class="form-control">
                                <!-- <option value="0">Parent Category</option> -->
                                    @foreach($levels as $val)
                                      <option value="{{$val->id}}" @if($data->parent_id=='$val->id') selected @endif >{{$val->name}}</option>
                                    @endforeach
                               </select>
                              </div>

                              <div class="form-group">
                                
                                 <label>Category Url</label>
                                 <input type="text" class="form-control" value="{{$data->category_url}}" name="category_url">
                              </div>
                              
                              <div class="form-group">
                                 <label>Description</label>
                                 <input type="text" class="form-control" value="{{$data->description}}" name="description">
                              </div>
                                                    
                              <div class="reset-button">
                                 <!-- <a href="#" class="btn btn-success">Save</a> -->
                                 <input type="submit" name="submit" value="Update" class="btn btn-success">
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection



