@extends('admin.layout.master')
@section('content')
	<!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-user-o"></i>
               </div>
               <div class="header-title">
                  <h1>Category List</h1>
                  <small>Category Details</small>
               </div>
            </section>
            @if(session('success_message'))

              <div class="alert alert-warning alert-dismissible " role="alert">
                  <strong>{{session('success_message')}}</strong>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                  </button>               
               </div>
            @endif
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Category List</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                        <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="btn-group">
                              <div class="buttonexport"> 
                                 <a href="{{url('/add_category')}}" class="btn btn-add"><i class="fa fa-plus"></i> Add Category</a>  
                              </div>
                              
                              <ul class="dropdown-menu exp-drop" role="menu">
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'false'});"> 
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});">
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON (ignoreColumn)</a>
                                 </li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'true'});">
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON (with Escape)</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'xml',escape:'false'});">
                                    <img src="assets/dist/img/xml.png" width="24" alt="logo"> XML</a>
                                 </li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'sql'});"> 
                                    <img src="assets/dist/img/sql.png" width="24" alt="logo"> SQL</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'csv',escape:'false'});"> 
                                    <img src="assets/dist/img/csv.png" width="24" alt="logo"> CSV</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'txt',escape:'false'});"> 
                                    <img src="assets/dist/img/txt.png" width="24" alt="logo"> TXT</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'excel',escape:'false'});"> 
                                    <img src="assets/dist/img/xls.png" width="24" alt="logo"> XLS</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'doc',escape:'false'});">
                                    <img src="assets/dist/img/word.png" width="24" alt="logo"> Word</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'powerpoint',escape:'false'});"> 
                                    <img src="assets/dist/img/ppt.png" width="24" alt="logo"> PowerPoint</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'png',escape:'false'});"> 
                                    <img src="assets/dist/img/png.png" width="24" alt="logo"> PNG</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> 
                                    <img src="assets/dist/img/pdf.png" width="24" alt="logo"> PDF</a>
                                 </li>
                              </ul>
                           </div>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <form action="{{url('/delete_multiple_category')}}" method="post">
                                 @csrf
                                 <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                    <thead>
                                       <tr class="info">
                                          <th>Select</th>
                                          <th>Parent Id</th>
                                          <th>Name</th>
                                          <th>Url</th>
                                          <th>Description</th>
                                          <th>Status</th>
                                          <th>Action</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach($data as $data)
                                       <tr>
                                          <td>
                                             <input type="checkbox" name="delete_id[]" value="{{$data->id}}">
                                          </td>
                                          <td>{{$data->parent_id}}</td>
                                          <td>{{$data->name}}</td>
                                          <td>{{$data->category_url}}</td>
                                          <td>{{$data->description}}</td>
                                          <td><input type="checkbox" class="category_status" data-id="<?php echo $data['id'] ?>" <?php if($data['status']==1) {echo "checked";} ?> name="status"></td>
                                          <td>
                                             <a href="{{url('edit_category/'.$data->id)}}" class="btn btn-success btn-sm"><i class="fa fa-pencil" aria-hidden="true" title="Edit"></i></a>
                                             <a  data-toggle="modal" href="#myModal{{$data->id}}" class="btn btn-warning btn-sm "><i class="fas fa-eye" aria-hidden="true" title="view"></i></a>
                                             <a href="{{url('delete_category/'.$data->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" title="Delete"></i></a>
                                          </td>
                                       </tr> 
                                       <div class="modal fade" id="myModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="color: #428bca;" aria-hidden="true">
                                          <div class="modal-dialog">
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                   <h4 class="modal-title" id="myModalLabel">{{$data->name}} Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                  <p>Category Url: {{$data->category_url}}</p>
                                                  <p>Description: {{$data->description}}</p>
                                                  
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                             </div>
                                          </div>  
                                       </div>

                                       @endforeach
                                    </tbody>
                                 </table>
                                 <button type="submit" class="btn btn-danger">Delete Selected</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection