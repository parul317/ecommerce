@extends('admin.layout.master')
@section('content')
   <!-- Content Wrapper. Contains page content -->

         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-clock-o"></i>
               </div>
               <div class="header-title">
                  <h1>Order Details</h1>
                  <small>Order Details</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-6">
                     <div class="panel lobidisable panel-bd">
                        <div class="panel-heading">
                           <div class="panel-title">
                              <h4>Order details</h4>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-bordered table-hover">
                                 <tbody>
                                    <tr>
                                       <td>Order Date</td>
                                       <td>{{$orderDetails->created_at}}</td>
                                    </tr>
                                    <tr>
                                       <td>Order Status</td>
                                       <td>{{$orderDetails->order_status}}</td>
                                    </tr>
                                    <tr>
                                       <td>Order Total</td>
                                       <td>{{$orderDetails->grand_total}}</td>
                                    </tr>
                                    <tr>
                                       <td>Shipping Charges</td>
                                       <td>{{$orderDetails->shipping_charges}}</td>
                                    </tr>
                                    <tr>
                                       <td>Coupon Code </td>
                                       <td>{{$orderDetails->coupone_code}}</td>
                                    </tr>
                                    <tr>
                                       <td>Coupon Amount</td>
                                       <td>{{$orderDetails->coupone_amount}}</td>
                                    </tr>
                                    <tr>
                                       <td>Payment Method</td>
                                       <td>{{$orderDetails->payment_method}}</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel lobidisable panel-bd">
                        <div class="panel-heading">
                           <div class="panel-title">
                              <h4>Customer Details</h4>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-bordered table-hover">
                                 
                                 <tbody>
                                    <tr>
                                       <td>Customer Name</td>
                                       <td>{{$userDetails->name}}</td>
                                       
                                    </tr>
                                    <tr>
                                       <td>Customer Email</td>
                                       <td>{{$userDetails->email}}</td>
                                       
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>

                     <div class="panel lobidisable panel-bd">
                        <div class="panel-heading">
                           <div class="panel-title">
                              <h4>Order Status Update</h4>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-bordered table-hover">
                                 <tbody>
                                    <form class="col-sm-6" method="post" action="" enctype="multipart/form-data">
                                       @csrf
                                       <div class="form-group">
                                          <select class="form-control" name="payment_method">
                                             <option>select</option>
                                             <option>new</option>
                                             <option>pending</option>
                                             <option>cancled</option>
                                             <option>Confirmed</option>
                                             <option>Deliverd</option>
                                             <option>Complete</option>
                                          </select>
                                       </div>
                                                             
                                       <div class="reset-button">                                         
                                          <input type="submit" name="submit" value="Update" class="btn btn-primary">
                                       </div>
                                    </form>
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-sm-6">
                     <div class="panel lobidisable panel-bd">
                        <div class="panel-heading">
                           <div class="panel-title">
                              <h4>Billing Address</h4>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-bordered table-hover">
                                 <tbody>
                                    <tr>
                                       <td>Name</td>
                                       <td>{{$userDetails->name}}</td>
                                    </tr>
                                    <tr>
                                       <td>Address</td>
                                       <td>{{$userDetails->address}}</td>
                                    </tr>
                                    <tr>
                                       <td>City</td>
                                       <td>{{$userDetails->city}}</td>
                                    </tr>
                                    <tr>
                                       <td>State</td>
                                       <td>{{$userDetails->state}}</td>
                                    </tr>
                                    <tr>
                                       <td>Country</td>
                                       <td>{{$userDetails->country}}</td>
                                    </tr>
                                    <tr>
                                       <td>Pincode</td>
                                       <td>{{$userDetails->pincode}}</td>
                                    </tr>
                                    <tr>
                                       <td>Mobile</td>
                                       <td>{{$userDetails->mobile}}</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel lobidisable panel-bd">
                        <div class="panel-heading">
                           <div class="panel-title">
                              <h4>Shipping Address</h4>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-bordered table-hover">
                                 <tbody>
                                    <tr>
                                       <td>Name</td>
                                       <td>{{$orderDetails->name}}</td>
                                    </tr>
                                    <tr>
                                       <td>Address</td>
                                       <td>{{$orderDetails->address}}</td>
                                    </tr>
                                    <tr>
                                       <td>City</td>
                                       <td>{{$orderDetails->city}}</td>
                                    </tr>
                                    <tr>
                                       <td>State</td>
                                       <td>{{$orderDetails->state}}</td>
                                    </tr>
                                    <tr>
                                       <td>Country</td>
                                       <td>{{$orderDetails->country}}</td>
                                    </tr>
                                    <tr>
                                       <td>Pincode</td>
                                       <td>{{$orderDetails->pincode}}</td>
                                    </tr>
                                    <tr>
                                       <td>Mobile</td>
                                       <td>{{$orderDetails->mobile}}</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection