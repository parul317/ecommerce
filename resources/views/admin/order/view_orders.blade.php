@extends('admin.layout.master')
@section('content')
	<!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-user-o"></i>
               </div>
               <div class="header-title">
                  <h1>Orders List</h1>
                  <small>Orders Details</small>
               </div>
            </section>
            @if(session('success_message'))

              <div class="alert alert-warning alert-dismissible " role="alert">
                  <strong>{{session('success_message')}}</strong>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                  </button>               
               </div>
            @endif
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Order List</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                        <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="btn-group">
                              <!-- <div class="buttonexport"> 
                                 <a href="{{url('/add_coupon')}}" class="btn btn-add"><i class="fa fa-plus"></i> Add Coupon</a>  
                              </div> -->
                              
                              <ul class="dropdown-menu exp-drop" role="menu">
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'false'});"> 
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});">
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON (ignoreColumn)</a>
                                 </li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'true'});">
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON (with Escape)</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'xml',escape:'false'});">
                                    <img src="assets/dist/img/xml.png" width="24" alt="logo"> XML</a>
                                 </li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'sql'});"> 
                                    <img src="assets/dist/img/sql.png" width="24" alt="logo"> SQL</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'csv',escape:'false'});"> 
                                    <img src="assets/dist/img/csv.png" width="24" alt="logo"> CSV</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'txt',escape:'false'});"> 
                                    <img src="assets/dist/img/txt.png" width="24" alt="logo"> TXT</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'excel',escape:'false'});"> 
                                    <img src="assets/dist/img/xls.png" width="24" alt="logo"> XLS</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'doc',escape:'false'});">
                                    <img src="assets/dist/img/word.png" width="24" alt="logo"> Word</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'powerpoint',escape:'false'});"> 
                                    <img src="assets/dist/img/ppt.png" width="24" alt="logo"> PowerPoint</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'png',escape:'false'});"> 
                                    <img src="assets/dist/img/png.png" width="24" alt="logo"> PNG</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> 
                                    <img src="assets/dist/img/pdf.png" width="24" alt="logo"> PDF</a>
                                 </li>
                              </ul>
                           </div>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              
                                 <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                    <thead>
                                       <tr class="info">
                                       	  <th>#</th>
                                          <th>Details</th>
                                          <th>Order Date</th>
                                          <th>Order Status</th>
                                          <th>Grand Total</th>
                                          <th>Payment Method</th>
                                          
                                          <th>Action</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    		$key = 0;
                                    	?>
                                    	@foreach($data as $data)
                                       <tr>
                                          <td><?php $key = $key+1; echo $key ?></td>
                                          <td><p>Order No: {{$data->id}}</p>
                                          	  <p>Name: {{$data->name}}</p>
                                          	  <p>Contact: {{$data->mobile}}</p>
                                          	  <p>Email: {{$data->user_email}}</p>
                                          </td>
                                          <td>{{$data->created_at}}</td>
                                          <td>{{$data->order_status}}</td>
                                          <td>{{$data->grand_total}}</td>
                                          <td>{{$data->payment_method}}</td>
                                          
                                          <td>
                                             <a href="{{url('/admin/orders/'.$data->id)}}" class="btn btn-add btn-sm">View Order Details</a>
                                          </td>
                                       </tr> 

                                       @endforeach
                                    </tbody>
                                 </table>
                                 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection