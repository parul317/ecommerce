@extends('admin.layout.master')
@section('content')
	
	<!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-users"></i>
               </div>
               <div class="header-title">
                  <h1>Update banner</h1>
                  <small>Update banner</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="{{url('view_banner')}}"> 
                              <i class="fa fa-list"></i>  Banner List </a>  
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" method="post" action="{{url('/edit_banner/'.$data->id)}}" enctype="multipart/form-data">
                              @csrf
                              <div class="form-group">
                              	
                                 <label>Url</label>
                                 <input type="text" class="form-control" name="url" value="{{$data->url}}">
                              </div>
                              
                              <div class="form-group">
                                 <label>Title</label>
                                 <input type="text" name="title" class="form-control" value="{{$data->title}}">
                              </div>

                              <div class="form-group">
                                 <label>SubTitle</label>
                                 <input type="text" name="subtitle" class="form-control" value="{{$data->subtitle}}">
                              </div>

                              <div class="form-group">
                                 <label>Picture upload</label>
                                 <input type="hidden" name="current_image" value="{{$data->image}}">
                                 <input type="file" name="image" class="form-control">
                              </div>
                                                    
                              <div class="reset-button">
                                 <!-- <a href="#" class="btn btn-success">Save</a> -->
                                 <input type="submit" name="submit" value="Save" class="btn btn-success">
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection