@extends('admin.layout.master')
@section('content')
	
	<!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-users"></i>
               </div>
               <div class="header-title">
                  <h1>Add Coupon</h1>
                  <small>Add Coupon</small>
               </div>
            </section>
            @if(session('message'))

           <p class ="alert alert-success">
            {{session('message')}}
           </p>
            
            @endif
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="{{url('view_coupon')}}"> 
                              <i class="fa fa-list"></i>  coupon List </a>  
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" method="post" action="{{url('/add_coupon')}}" enctype="multipart/form-data">
                              @csrf
                              <div class="form-group">
                                 
                                 <label>Coupon Code</label>
                                 <input type="text" class="form-control"  name="coupon_code">
                              </div>

                              

                              <div class="form-group">
                              	
                                 <label>Amount</label>
                                 <input type="text" class="form-control"  name="amount">
                              </div>

                              <div class="form-group">
                               <label>Amount Type</label>  
                               <select name="amount_type" class="form-control">
                                  <option value="0">Select</option>
                                  
                                    <option value="Percentage">Percentage</option>
                                    <option value="Fixed">Fixed</option>
                                
                               </select>
                              </div>
                              
                              <div class="form-group">
                                 <label>Expiry Date</label>
                                 <input type="date" class="form-control"  name="expiry_date">
                              </div>

                                                    
                              <div class="reset-button">
                                 <!-- <a href="#" class="btn btn-success">Save</a> -->
                                 <input type="submit" name="submit" value="Save" class="btn btn-success">
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection