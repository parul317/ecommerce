@extends('admin.layout.master')
@section('content')
	<!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-user-o"></i>
               </div>
               <div class="header-title">
                  <h1>Product List</h1>
                  <small>Product Details</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Product List</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                        <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="btn-group">
                              <div class="buttonexport"> 
                                 <a href="{{url('/add_product')}}" class="btn btn-add"><i class="fa fa-plus"></i> Add Product</a>  
                              </div>
                              
                              <ul class="dropdown-menu exp-drop" role="menu">
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'false'});"> 
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});">
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON (ignoreColumn)</a>
                                 </li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'json',escape:'true'});">
                                    <img src="assets/dist/img/json.png" width="24" alt="logo"> JSON (with Escape)</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'xml',escape:'false'});">
                                    <img src="assets/dist/img/xml.png" width="24" alt="logo"> XML</a>
                                 </li>
                                 <li><a href="#" onclick="$('#dataTableExample1').tableExport({type:'sql'});"> 
                                    <img src="assets/dist/img/sql.png" width="24" alt="logo"> SQL</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'csv',escape:'false'});"> 
                                    <img src="assets/dist/img/csv.png" width="24" alt="logo"> CSV</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'txt',escape:'false'});"> 
                                    <img src="assets/dist/img/txt.png" width="24" alt="logo"> TXT</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'excel',escape:'false'});"> 
                                    <img src="assets/dist/img/xls.png" width="24" alt="logo"> XLS</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'doc',escape:'false'});">
                                    <img src="assets/dist/img/word.png" width="24" alt="logo"> Word</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'powerpoint',escape:'false'});"> 
                                    <img src="assets/dist/img/ppt.png" width="24" alt="logo"> PowerPoint</a>
                                 </li>
                                 <li class="divider"></li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'png',escape:'false'});"> 
                                    <img src="assets/dist/img/png.png" width="24" alt="logo"> PNG</a>
                                 </li>
                                 <li>
                                    <a href="#" onclick="$('#dataTableExample1').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> 
                                    <img src="assets/dist/img/pdf.png" width="24" alt="logo"> PDF</a>
                                 </li>
                              </ul>
                           </div>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <form action="{{url('/delete_multiple_product')}}" method="post">
                                 @csrf
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Select</th>
                                       <th>Product Name</th>
                                       <th>Code</th>
                                       <th>Price</th>
                                       <th>Status</th>
                                       <th>Image</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 	@foreach($data as $data)
                                    <tr>
                                       <td>
                                          <input type="checkbox" name="delete_id[]" value="{{$data->id}}">
                                       </td>
                                                   
                                       <td>{{$data->product_name}}</td>
                                       <td>{{$data->product_code}}</td>
                                       <td>{{$data->price}}</td>
                                       <td><input type="checkbox" class="product_status" data-id="<?php echo $data['id'] ?>" <?php if($data['status']==1) {echo "checked";} ?> name="status">
                                       </td>
                                       <td><img src="{{ url('/upload/'.$data->image) }}" style="height: 140px; width: 120px;">
                                       </td>
                                       <td>
                                          <a href="{{url('/edit_product/'.$data->id)}}" class="btn btn-success btn-sm"><i class="fa fa-pencil" aria-hidden="true" title="Edit"></i></a>
                                          <a  data-toggle="modal" href="#myModal{{$data->id}}" class="btn btn-warning btn-sm "><i class="fas fa-eye" aria-hidden="true" title="view"></i></a>
                                          <a href="{{url('/delete_product/'.$data->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" title="Delete"></i></a>
                                          <a href="{{url('/add_images/'.$data->id)}}" class="btn btn-primary btn-sm">Add image</a>
                                       </td>
                                    </tr>
                                    <div class="modal fade" id="myModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="color: #428bca;" aria-hidden="true">
                                          <div class="modal-dialog">
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                   <h4 class="modal-title" id="myModalLabel">{{$data->product_name}} Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="container-fluid">
                                                      <div class="row">
                                                         <div class="col-sm-8">
                                                  <p>Product Code: {{$data->product_code}}</p>
                                                  <p>Size: {{$data->size}}</p>
                                                  <p>Price: {{$data->price}}</p>
                                                  <p>Quantity: {{$data->quantity}}</p>
                                                  <p>Description: {{$data->description}}</p>
                                                  <p>Featured product: <?php 
                                                      if($data['featured_product']==1) {echo "Yes";} 
                                                      else { echo "No" ;}
                                                   ?>
                                                   </p>
                                                   <p>Popular product: <?php 
                                                      if($data['popular_product']==1) {echo "Yes";} 
                                                      else { echo "No" ;}
                                                   ?>
                                                   </p>
                                                   <p>Latest product: <?php 
                                                      if($data['latest_product']==1) {echo "Yes";} 
                                                      else { echo "No" ;}
                                                   ?>
                                                   </p>
                                                   </div>
                                                   <div class="col-sm-4">
                                                      <img src="{{ url('/upload/'.$data->image) }}" style="width: 130px; height: 150px;">
                                                   </div>
                                                   </div>
                                                   </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                             </div>
                                          </div>  
                                       </div>
                                    @endforeach
                                 </tbody>
                              </table>
                              <button type="submit" class="btn btn-danger">Delete Selected</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection