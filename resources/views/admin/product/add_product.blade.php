@extends('admin.layout.master')
@section('content')
	
	<!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-users"></i>
               </div>
               <div class="header-title">
                  <h1>Add Product</h1>
                  <small>Add Product</small>
               </div>
            </section>
            @if(session('message'))

           <p class ="alert alert-success">
            {{session('message')}}
           </p>
            
            @endif
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="{{url('view_product')}}"> 
                              <i class="fa fa-list"></i>  Product List </a>  
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" method="post" action="{{url('/add_product')}}" enctype="multipart/form-data">
                              @csrf
                              
                              <div class="form-group">
                               <label>Parent Category</label>  
                               <select name="cat_id" class="form-control">
                                  <option value="0">Select Category</option>
                                  @foreach($levels as $val)
                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                 @endforeach
                               </select>
                              </div>

                              <div class="form-group">
                                 
                                 <label>Product Name</label>
                                 <input type="text" class="form-control" placeholder="" name="product_name">
                              </div>

                              <div class="form-group">
                              	
                                 <label>Product Code</label>
                                 <input type="text" class="form-control" placeholder="" name="product_code">
                              </div>
                              
                              <div class="form-group">
                                 <label>Size</label>
                                 <input type="text" class="form-control"  name="size">
                              </div>

                              <div class="form-group">
                                 <label>Product Overview</label>
                                 <input type="text" class="form-control"  name="overview">
                              </div>

                              <div class="form-group">
                                 <label>Description</label>
                                 <input type="text" class="form-control" placeholder="" name="description">
                              </div>

                              <div class="form-group">
                                 <label>Price</label>
                                 <input type="text" class="form-control" placeholder="" name="price">
                              </div>

                              <div class="form-group">
                                 <label>Quantity</label>
                                 <input type="text" class="form-control" placeholder="" name="quantity">
                              </div>

                               <div class="form-group">
                                  <label>featured_product</label>
                                    <select  name="featured_product" class="form-control" >  
                                         <option value="1">Yes</option>
                                         <option value="0">No</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                  <label>Popular Product</label>
                                    <select  name="popular_product" class="form-control" >  
                                         <option value="1">Yes</option>
                                         <option value="0">No</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                  <label>Latest Product</label>
                                    <select  name="latest_product" class="form-control" >  
                                         <option value="1">Yes</option>
                                         <option value="0">No</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                  <label>Upload Image</label>
                                  <input type="file" name="image" class="form-control">
                                </div>
                                                    
                              <div class="reset-button">
                                 <!-- <a href="#" class="btn btn-success">Save</a> -->
                                 <input type="submit" name="submit" value="Save" class="btn btn-success">
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection