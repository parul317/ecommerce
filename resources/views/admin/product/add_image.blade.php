@extends('admin.layout.master')
@section('content')
	
	<!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-users"></i>
               </div>
               <div class="header-title">
                  <h1>Add Product Image</h1>
                  <small>Add Product Image</small>
               </div>
            </section>
            @if(session('message'))

           <p class ="alert alert-success">
            {{session('message')}}
           </p>
            
            @endif
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="{{url('view_product')}}"> 
                              <i class="fa fa-list"></i>  Product List </a>  
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" method="post" action="{{url('/add_images/'.$product->id)}}" enctype="multipart/form-data">
                              @csrf
                              <input type="hidden" name="product_id" value="{{$product->id}}">
                              <div class="form-group">
                                 
                                 <label>Product Name</label>
                                 <input type="text" class="form-control"  name="product_name" value="{{$product->product_name}}" disabled="">
                              </div>

                              <div class="form-group">
                              	
                                 <label>Product Code</label>
                                 <input type="text" class="form-control"  name="product_code" value="{{$product->product_code}}" disabled="">
                              </div>

                                <!-- <div class="form-group">
                                  <label>Upload Image</label>
                                  <input type="file" name="image[]"  class="form-control" multiple="multiple">
                                </div> -->

                                <div class="form-group">
                                  <label>Upload Image</label>
                                  <input type="file" name="image"  class="form-control">
                                </div>
                                                    
                              <div class="reset-button">
                                 <!-- <a href="#" class="btn btn-success">Save</a> -->
                                 <input type="submit" name="submit" value="Save" class="btn btn-success">
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->


            @if(session('flash_message_success'))

           <p class ="alert alert-success">
            {{session('flash_message_success')}}
           </p>
            
            @endif
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Product Images</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <form action="{{url('/delete_multiple_images')}}" method="post">
                                 @csrf
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Select</th>
                                       <th>Image</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                  @foreach($images as $image)
                                    <tr>
                                       <td>
                                          <input type="checkbox" name="delete_id[]" value="{{$image->id}}">
                                       </td>
                                       <td><img src="{{ url('/upload/product/'.$image->image) }}" style="height: 140px; width: 120px;"></td>
                                       <td>
                                          <a href="{{url('/delete_image/'.$image->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" title="Delete"></i></a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                              <button type="submit" class="btn btn-danger">Delete Selected</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
@endsection