@extends('front.layout.master')
@section('content')

<section class="main-container col1-layout">
    <div class="main container">
      <div class="col-lg-2 col-sm-12"></div>
      <div class="col-lg-8 col-sm-12">
        <div class="account-login">
        <div class="page-title">
          <h2>Thank You For Purchasing With Us!!</h2>
        </div>
        <h2>YOUR COD ORDER HAS BEEN PLACED</h2>
		<p>Your order number is {{Session::get('order_id')}} and total payable about is Rs. {{Session::get('grand_total')}}</p>
      </div>
      </div>
      <div class="col-lg-2 col-sm-12"></div>
    </div>
  </section>


<?php

Session::forget('order_id');
Session::forget('grand_total');

?>
@endsection