@extends('front.layout.master')
@section('content')

<body class="checkout-page">
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">


          <ul>
            <li class="home"> <a href="index.html" title="Go to Home Page">Home</a> <span>/</span></li>
            <li> <strong>Checkout</strong> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- main-container -->
  <div class="main-container col1-layout">
    <div class="main container">
      <div class="row">
        <div class="col-sm-12">
          <div class="product-area">
            <div class="content-tab-product-category"> 
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane  fade in active" id="checkout"> 
                  <!-- Checkout are start-->
                  <div class="checkout-area">
                    <div class="">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="row">
                            <div class="col-md-6 col-xs-12">
                              <div class="billing-details">
                                <div class="contact-text right-side">
                                  <h2>Billing To</h2>
                                  <form action="{{url('/checkout')}}" method="post">@csrf
                                    <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Billing Name</label>
                                          <input type="text" name="billingName" id="billingName" @if(!empty($userDetails->name)) value="{{$userDetails->name}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Billing Address</label>
                                          <input type="text" name="billingAddress" id="billingAddress" @if(!empty($userDetails->address)) value="{{$userDetails->address}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Billing City</label>
                                          <input type="text" name="billingCity" id="billingCity" @if(!empty($userDetails->city)) value="{{$userDetails->city}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Billing State</label>
                                          <input type="text" name="billingState" id="billingState" @if(!empty($userDetails->state)) value="{{$userDetails->state}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Country <em>*</em></label>
                                          <select class="selectpicker select-custom" data-live-search="true" name="billingCountry" id="billingCountry">
                                            
                                            @foreach($countries as $country)
  					  						                  <option value="{{$country->country_name}}" @if(!empty($userDetails->country) && $country->country_name == $userDetails->country) selected @endif>{{$country->country_name}}</option>
  					  						                  @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Billing Pincode</label>
                                          <input type="text" name="billingPincode" id="billingPincode" @if(!empty($userDetails->pincode)) value="{{$userDetails->pincode}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Billing Mobile</label>
                                          <input type="text" name="billingMobile" id="billingMobile" @if(!empty($userDetails->mobile)) value="{{$userDetails->mobile}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="create-acc clearfix">
                                            <input type="checkbox" id="billtoship">
                                            <label for="acc-toggle">Shipping Address Same as Billing Address</label>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                              <div class="billing-details">
                                <div class="contact-text right-side">
                                  <h2>Shipping To</h2>
                                    <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Shipping Name</label>
                                          <input type="text" name="shippingName" id="shippingName" @if(!empty($shippingDetails->name)) value="{{$shippingDetails->name}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Shipping Address</label>
                                          <input type="text" name="shippingAddress" id="shippingAddress" @if(!empty($shippingDetails->address)) value="{{$shippingDetails->address}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Shipping City</label>
                                          <input type="text" name="shippingCity" id="shippingCity" @if(!empty($shippingDetails->city)) value="{{$shippingDetails->city}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Shipping State</label>
                                          <input type="text" name="shippingState" id="shippingState" @if(!empty($shippingDetails->state)) value="{{$shippingDetails->state}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Country <em>*</em></label>
                                          <select class="selectpicker select-custom" data-live-search="true" name="shippingCountry" id="shippingCountry">
                                            <option value="1">Shipping Country</option>
                                            @foreach($countries as $country)
  					  						                  <option value="{{$country->country_name}}" @if(!empty($shippingDetails->country) && $country->country_name == $shippingDetails->country) selected @endif>{{$country->country_name}}</option>
  					  						                  @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Shipping Pincode</label>
                                          <input type="text" name="shippingPincode" id="shippingPincode" @if(!empty($shippingDetails->pincode)) value="{{$shippingDetails->pincode}}" @endif class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Shipping Mobile</label>
                                           <input type="text" name="shippingMobile" id="shippingMobile"@if(!empty($shippingDetails->mobile)) value="{{$shippingDetails->mobile}}" @endif  class="info">
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="create-acc clearfix">
                                          <div class="buttons-set">
                								            <a href="{{url('/order_review')}}"><button type="submit" class="button login" id="submit"><span>Checkout</span></button></a>
              								            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Checkout are end--> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--End main-container -->

  @endsection