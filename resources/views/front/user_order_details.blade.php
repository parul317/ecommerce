@extends('front.layout.master')
@section('content')

<div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <section class="col-sm-12 col-xs-12">
          <div class="col-main">
            <div class="my-account">
              <div class="page-title">
                <h2>View Order</h2>
              </div>
              <div class="dashboard">
                <div class="recent-orders">
                  <div class="table-responsive">
                    <table class="table table-striped table-responsive table-bordered text-left my-orders-table">
                      <thead>
                        <tr class="first last">
                          <th>Product Code</th>
                          <th>Product Name</th>
                          <th>Product Price</th>
                          
                          <th>Product Quantity</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                      	@foreach($orderDetails as $order)
                      	<tr>
                          	<td>{{$order->product_code}}</td>
                          	<td>{{$order->product_name}}
                          	    	
                          	</td>
							<td>{{$order->product_price}}</td>
                          	<td><span class="price">{{$order->product_qty}}</span></td>
                       	</tr>
						@endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
</div>
</div>

@endsection