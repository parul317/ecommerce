@extends('front.layout.master')
@section('content')

<!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a href="index.html" title="Go to Home Page">Home</a> <span>/</span></li>
            <li> <strong>Shopping cart </strong> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End -->
             


  <section class="main-container col1-layout">
    <div class="main container">
      <div class="row">
        <div class="col-sm-12 col-xs-12">
          @if(Session::has('flash_message_error'))
            <div class="alert alert-sm alert-danger alert-block" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{!! session('flash_message_error') !!}</strong>
            </div>
            @endif
            @if(Session::has('flash_message_success'))
            <div class="alert alert-sm alert-success alert-block" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{!! session('flash_message_success') !!}</strong>
            </div>
          @endif
          <div class="product-area">
            <div class="title-tab-product-category">
              <div class="text-center">
                <ul class="nav jtv-heading-style" role="tablist">
                  <li role="presentation" class="active"><a href="#cart" aria-controls="cart" role="tab" data-toggle="tab">1 Shopping cart</a></li>
                  <li role="presentation" class=""><a href="#checkout" aria-controls="checkout" role="tab" data-toggle="tab">2 Checkout</a></li>
                  <li role="presentation" class=""><a href="#complete-order" aria-controls="complete-order" role="tab" data-toggle="tab">3 Complete Order</a></li>
                </ul>
              </div>
            </div>
            <div class="content-tab-product-category"> 
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="cart"> 
                  <!-- cart are start-->
                  <div class="cart-page-area">
                    
                      <div class="table-responsive">
                        <table class="shop-cart-table">
                          <thead>

                            <tr>
                              <th class="product-thumbnail ">Image</th>
                              <th class="product-name ">Product Name</th>
                              <th class="product-price ">Unit Price</th>
                              <th class="product-quantity">Quantity</th>
                              <th class="product-subtotal ">Total</th>
                              <th class="product-remove">Remove</th>
                            </tr>
                          </thead>
                          <tbody> 
                            <?php $total_amount = 0; ?>

                            @foreach($userCart as $cartdata)
                            <tr class="cart_item">
                              <td class="item-img"><a href="#"><img src="{{asset('/upload/'.$cartdata->image)}}" alt="Product tilte is here "> </a></td>
                              <td class="item-title"><a href="#">{{$cartdata->product_name}} </a></td>
                              <td class="item-price"> {{$cartdata->price}} </td>
                              <td class="item-qty"><div class="cart-quantity">
                                  <div class="product-qty">
                                    <div class="cart-quantity">
                                      <div class="cart-plus-minus">

                                        <div class="dec qtybutton"> 
                                          <a href="{{url('/cart/update-quantity/'.$cartdata->id.'/1')}}" style="font-size: 25px;">+</a>
                                        </div>

                                        <input value="{{$cartdata->quantity}}" name="quantity" class="cart-plus-minus-box" type="text">

                                        <div class="dec qtybutton"> 
                                        @if($cartdata->quantity>1)
                                        <a href="{{url('/cart/update-quantity/'.$cartdata->id.'/-1')}}" style="font-size: 25px;">-</a>
                                        @endif
                                      </div>

                                        <!-- <div class="dec qtybutton">-</div>
                                        
                                        <div class="inc qtybutton">+</div> -->
                                      </div>
                                    </div>
                                  </div>
                                </div></td>
                              <td class="total-price"><strong> {{$cartdata->price*$cartdata->quantity}}</strong></td>
                              <td class="remove-item"><a href="{{url('/cart/delete/' .$cartdata->id)}}"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            
                            
                            <?php $total_amount = $total_amount + ($cartdata->price*$cartdata->quantity); ?>


                            @endforeach
                          </tbody>
                        </table>
                      </div>
                      <div class="cart-bottom-area">
                        <div class="row">
                          <div class="col-md-8 col-sm-7 col-xs-12">
                            <div class="update-coupne-area">
                              <div class="update-continue-btn text-right">
                                <button class="button btn-continue" title="Continue Shopping" type="button"><span>Continue Shopping</span></button>
                                <button class="button btn-empty" title="Clear Cart" value="empty_cart" name="update_cart_action" type="submit"><span>Clear Cart</span></button>
                                <button class="button btn-update" title="Update Cart" value="update_qty" name="update_cart_action" type="submit"><span>Update Cart</span></button>
                              </div>
                              <div class="coupn-area">
                                <form action="{{url('/cart/apply-coupon')}}" method="post" >
                                  @csrf
                                <div class="discount">
                                  <h3>Discount Codes</h3>
                                  <label for="coupon_code">Enter your coupon code if you have one.</label>
                                  <input type="hidden" value="0" id="remove-coupone" name="remove">
                                  <input type="text" value="" name="coupon_code" id="coupon_code" class="input-text fullwidth">
                                  <button value="Apply Coupon" class="button coupon " title="Apply Coupon" type="submit"><span>Apply Coupon</span></button>
                                </div>
                              </form>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-5 col-xs-12">
                            <div class="cart-total-area">
                              <div class="catagory-title cat-tit-5 text-right">
                                <h3>Cart Totals</h3>
                              </div>
                              <div class="sub-shipping">
                              @if(!empty(Session::get('CouponAmount')))
                                <p>Subtotal <span>₹ <?php echo $total_amount; ?></span></p>
                                <p>Coupon Discount <span>₹ <?php echo Session::get('CouponAmount'); ?></span></p>
                              </div>
                              <!-- <div class="shipping-method text-right">
                                
                              </div> -->
                              <div class="process-cart-total">
                                <p>Total <span>₹ <?php echo $total_amount - Session::get('CouponAmount'); ?></span></p>
                              </div>
                              @else
                              <div class="process-cart-total">
                                <p>Total <span>₹ <?php echo $total_amount - Session::get('CouponAmount'); ?></span></p>
                              </div>
                              @endif
                                                           
                              <div class="process-checkout-btn text-right">
                                @if(Auth::check())
                                  <a href="{{url('/checkout')}}"><button class="button btn-proceed-checkout" title="Proceed to Checkout" type="button"><span>Proceed to Checkout</span></button></a>
                                @else
                                  <a href="{{url('/login-register')}}"><button class="button btn-proceed-checkout" title="Proceed to Checkout" type="button"><span>Proceed to Checkout</span></button></a>
                                @endif
                              </div>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- cart are end--> 
                </div>
                <div role="tabpanel" class="tab-pane  fade in " id="checkout"> 
                  <!-- Checkout are start-->
                  <div class="checkout-area">
                    <div class="">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="coupne-customer-area mb50">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-checkout">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <h4 class="panel-title"> <img src="images/acc.jpg" alt=""> Returning customer? <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Click here to login </a> </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                  <div class="panel-body">
                                    <div class="sm-des"> If you have shopped with us before, please enter your details in the boxes below. If you are a new customer please proceed to the Billing & Shipping section. </div>
                                    <div class="first-last-area">
                                      <div class="input-box">
                                        <label>User Name Or Email</label>
                                        <input type="email" placeholder="Your Email" class="info" name="email">
                                      </div>
                                      <div class="input-box">
                                        <label>Password</label>
                                        <input type="password" placeholder="Password" class="info" name="padd">
                                      </div>
                                      <div class="frm-action cc-area">
                                        <div class="input-box tci-box"> <a href="#" class="btn-def btn2">Login</a> </div>
                                        <span>
                                        <input type="checkbox" class="remr">
                                        Remember me </span> <a class="forgotten forg" href="#">Forgotten Password</a> </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Checkout are end--> 
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    <div class="jtv-crosssel-pro">
      <div class="jtv-new-title">
        <h2>you may be interested</h2>
      </div>
      <div class="category-products">
        <ul class="products-grid">
          <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class="item-inner">
              <div class="item-img">
                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                  <div class="new-label new-top-left">new</div>
                  <div class="sale-label sale-top-right">sale</div>
                  <div class="mask-shop-white"></div>
                  <div class="new-label new-top-left">new</div>
                  <a class="quickview-btn" href="quick-view.html"><span>Quick View</span></a> <a href="wishlist.html">
                  <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                  </a> <a href="compare.html">
                  <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                  </a> </div>
              </div>
              <div class="item-info">
                <div class="info-inner">
                  <div class="item-title"> <a title="Product tilte is here" href="#">Product tilte is here </a> </div>
                  <div class="item-content">
                    <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                    <div class="item-price">
                      <div class="price-box"> <span class="regular-price"> <span class="price">$75.00</span></span></div>
                    </div>
                    <div class="actions"><a href="#" class="link-wishlist" title="Add to Wishlist"></a>
                      <div class="add_cart">
                        <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                      </div>
                      <a href="#" class="link-compare" title="Add to Compare"></a> </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          
        </ul>
      </div>
    </div>
  </div>
  
  
@endsection