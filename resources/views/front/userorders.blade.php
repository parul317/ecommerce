@extends('front.layout.master')
@section('content')

<div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <section class="col-sm-12 col-xs-12">
          <div class="col-main">
            <div class="my-account">
              <div class="page-title">
                <h2>My Orders</h2>
              </div>
              <div class="dashboard">
                <div class="recent-orders">
                  <div class="table-responsive">
                    <table class="table table-striped table-responsive table-bordered text-left my-orders-table">
                      <thead>
                        <tr class="first last">
                          <th>Order Id</th>
                          <th>Ordered Product</th>
                          <th>Payment Method</th>
                          <th><span class="nobr">Grand Total</span></th>
                          <th>Order Date</th>
                          <th>delivery Status</th>
                          <th class="text-center">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      	@foreach($orders as $order1)
                      	<tr>
                          	<td>{{$order1->id}}</td>
                          	<td>
                          	    @foreach($order1->order as $pro)
                          	    <a href="{{url('/orders/'.$order1->id)}}">
                          	    	{{$pro->product_code}}
                          	    	({{$pro->product_qty}})
                          	    </a><br>
                          	    @endforeach 	
                          	</td>
							              <td>{{$order1->payment_method}}</td>
                          	<td><span class="price">{{$order1->grand_total}}</span></td>
                          	<td><span class="price">{{$order1->created_at}}</span></td>
                          	<td class="text-primary"><em>New</em></td>
                          	<td class="text-center last">
                          		<div class="btn-group"> 
                                <a href="{{url('/orders/'.$order1->id)}}" class="btn btn-view-order">View Order</a> 
                                <a href="#" class="btn btn-reorder btn-primary">Cancle Order</a> 
                              </div>
                          	</td>
                       	</tr>
						            @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
</div>
</div>

@endsection