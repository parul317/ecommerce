@extends('front.layout.master')
@section('content')

<body class="checkout-page">
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a href="index.html" title="Go to Home Page">Home</a> <span>/</span></li>
            <li> <strong>Order Review</strong> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  <!-- main-container -->
  <div class="main-container col1-layout">
    <div class="main container">
      <div class="row">
        <div class="col-sm-12">
          <div class="product-area">
            <div class="content-tab-product-category">  
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane  fade in active" id="checkout"> 
                  <!-- Checkout are start-->
                  <div class="checkout-area">
                    <div class="">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="row">
                            <div class="col-md-6 col-xs-12">
                              <div class="billing-details">
                                <div class="contact-text right-side">
                                  <h2>Billing Address</h2>
                                    <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Name : </label> {{$userDetails->name}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Address : </label> {{$userDetails->address}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> City : </label> {{$userDetails->city}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> State : </label> {{$userDetails->state}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label>Country : </label> {{$userDetails->country}}
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Pincode : </label> {{$userDetails->pincode}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Mobile : </label> {{$userDetails->mobile}}
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                              <div class="billing-details">
                                <div class="contact-text right-side">
                                  <h2>Shipping Details</h2>
                                    <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Name : </label> {{$shippingDetails->name}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Address : </label> {{$shippingDetails->address}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> City : </label> {{$shippingDetails->city}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> State : </label> {{$shippingDetails->state}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Country : </label> {{$shippingDetails->country}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Pincode : </label> {{$shippingDetails->pincode}}
                                        </div>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="input-box">
                                          <label> Mobile : </label> {{$shippingDetails->mobile}}
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Checkout are end--> 
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  
    <div class="container">
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="cart"> 
                  <!-- cart are start-->
                  <div class="cart-page-area">                   
                      <div class="table-responsive">
                        <table class="shop-cart-table">
                          <thead>

                            <tr>
                              <th class="product-thumbnail ">Image</th>
                              <th class="product-name ">Product Name</th>
                              <th class="product-price ">Unit Price</th>
                              <th class="product-quantity">Quantity</th>
                              <th class="product-subtotal ">Total</th>
                              <th class="product-remove">Remove</th>
                            </tr>
                          </thead>
                          <tbody> 
                            <?php $total_amount = 0; ?>

                            @foreach($userCart as $cartdata)
                            <tr class="cart_item">
                              <td class="item-img"><a href="#"><img src="{{asset('/upload/'.$cartdata->image)}}" alt="Product tilte is here "> </a></td>
                              <td class="item-title"><a href="#">{{$cartdata->product_name}} </a></td>
                              <td class="item-price"> {{$cartdata->price}} </td>
                              <td class="item-qty">{{$cartdata->quantity}}</td>
                              <td class="total-price"><strong> {{$cartdata->price*$cartdata->quantity}}</strong></td>
                              <td class="remove-item"><a href="{{url('/cart/delete/' .$cartdata->id)}}"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            
                            
                            <?php $total_amount = $total_amount + ($cartdata->price*$cartdata->quantity); ?>


                            @endforeach
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
    

              <div class="tab-content">
                <div role="tabpanel" class="tab-pane  fade in active" id="complete-order">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="checkout-payment-area">
                        <div class="checkout-total">
                          <h3>Your total</h3>
                          <form action="#" method="post">
                            <div class="table-responsive">
                             
                              <table class="checkout-area table">
                                <tbody>
                                  <tr class="cart_item check-item prd-name">
                                    <td class="ctg-type"> Cart Sub Total </td>
                                    <td class="cgt-des"> ₹ {{$total_amount}}</td>
                                  </tr>
                                  <tr class="cart_item check-item prd-name">
                                    <td class="ctg-type"> Shipping Charges(+) </td>
                                    <td class="cgt-des"> ₹ 0</td>
                                  </tr>
                                  <tr class="cart_item">
                                    <td class="ctg-type"> Coupon Discount(-) </td>
                                    <td class="cgt-des">@if(!empty(Session::get('CouponAmount'))) ₹ {{Session::get('CouponAmount')}} @else ₹ 0 @endif</td>
                                  </tr>
                                  <tr class="cart_item">
                                    <td class="ctg-type crt-total"> Grand Total </td>
                                    <td class="cgt-des prc-total"> ₹ {{$grand_total = $total_amount - Session::get('CouponAmount')}} </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </form>
                        </div>

                        <div class="payment-section">
                          <div class="pay-toggle">
                            <form action="{{url('/place-order')}}" name="paymentForm" id="paymentForm" method="post">
                              @csrf
                             <input type="hidden" name="grand_total" value="{{$grand_total}}">
                              <div class="pay-type">
                                <div class="form-check">
                                  <input type="radio" class="form-check-input cod" name="payment_method" id="credit" value="cod">
                                  <label for="cashOnDelivery">Cash on Delivery</label>
                                </div>
                                <div class="form-check">
                                  <input type="radio" class="form-check-input stripe" value="stripe" name="payment_method" id="debit">
                                  <label for="stripe">Stripe</label>
                                </div>
                                <div class="form-check">
                                  <input type="radio" class="form-check-input paytm" value="paytm" name="payment_method" id="debit">
                                  <label for="Paytm">Paytm</label>
                                </div>
                                <div class="form-check">
                                  <input type="radio" class="form-check-input Instamojo" value="Instamojo" name="payment_method" id="debit">
                                  <label for="Instamojo">Instamojo</label>
                                </div>
                                <div class="form-check">
                                  <input type="radio" class="form-check-input Razorpay" value="Razorpay" name="payment_method" id="debit">
                                  <label for="Razorpay">Razorpay</label>
                                </div>
                              </div>
                              <div class="input-box"><button type="submit" class="button login" onclick="return selectPaymentMethod();"><span>Place Order</span></button> </div>
                                                          </form>
                              </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
  </div>
  <!--End main-container -->

  @if(Session::has('amount'))
    <div class="container tex-center">
      <form action="/pay" method="POST">
        <script
            src="https://checkout.razorpay.com/v1/checkout.js"
            data-key="rzp_test_qAN8TZjVXcCBz2"
          data-amount="{{Session::get('amount')}}" 
            data-currency="INR"
          data-order_id="{{Session::get('order_id')}}"
            data-buttontext="Pay with Razorpay"
            data-name="Coffee"
            data-description="Test transaction"
            data-theme.color="#F37254"
        ></script>
        <input type="hidden" custom="Hidden Element" name="hidden">
      </form>
    </div>   
    @endif
</body>

  @endsection