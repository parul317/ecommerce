@extends('front.layout.master')
@section('content')

<section class="main-container col1-layout">
    <div class="main container">
      <div class="col-lg-3 col-sm-12"></div>
      <div class="col-lg-6 col-sm-12">
        <div class="account-login">
        <div class="page-title">
          <h2>My Details</h2>
        </div>
        <fieldset>
          <div class="registered-users"><strong>Change Details</strong>
            <div class="content">
              <form action="{{url('/change-address')}}" method="post">
                @csrf
              <ul class="form-list">
                <li>
                  <label for="name">Name<span class="required">*</span></label>
                  <input type="text" name="name" id="name" value="{{$UserDetails->name}}" class="input-text required-entry">
                </li>
                <li>
                  <label for="address">Address<span class="required">*</span></label>
                  <input type="text" name="address" id="address"  placeholder="Address" value="{{$UserDetails->address}}" class="input-text required-entry">
                </li>
                <li>
                  <label for="city">City <span class="required">*</span></label>
                  <input type="text" name="city" id="city" placeholder="City" value="{{$UserDetails->city}}" class="input-text required-entry">
                </li>
                <li>
                  <label for="state">State <span class="required">*</span></label>
                  <input type="text" name="state" id="state" placeholder="State" value="{{$UserDetails->state}}"  class="input-text required-entry">
                </li>
                <li>
                  <label for="country">Country <span class="required">*</span></label><br>
                  <select name="country" id="country" style="width: 100%;">
                    @foreach($country as $con)
                    <option value="{{$con->id}}" @if( $con->id == $UserDetails->country) selected @endif>{{$con->country_name}}</option>
                    @endforeach
                    
                  </select>
                </li>
                <li>
                  <label for="pincode">Pincode <span class="required">*</span></label>
                  <input type="text" name="pincode" id="pincode"  placeholder="Pincode" value="{{$UserDetails->pincode}}" class="input-text required-entry">
                </li>
                <li>
                  <label for="mobile">Mobile <span class="required">*</span></label>
                  <input type="text" name="mobile" id="mobile" placeholder="Mobile" value="{{$UserDetails->mobile}}" class="input-text required-entry">
                </li>
              </ul>
              <p class="required">* Required Fields</p>
              <div class="buttons-set">
                <button id="submit" type="submit" class="button login" value="Save"><span>Submit</span></button>
              </div>
            </form>
            </div>
          </div>
        </fieldset>
      </div>
      </div>
      <div class="col-lg-3 col-sm-12"></div>
    </div>
  </section>

  @endsection