@extends('front.layout.master')
@section('content')

<body class="login-page">
   @if(Session::has('flash_message_error'))
  <div class="alert alert-sm alert-danger alert-block" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
      <strong>{!! session('flash_message_error') !!}</strong>
  </div>
  @endif
  @if(Session::has('flash_message_success'))
  <div class="alert alert-sm alert-success alert-block" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
      <strong>{!! session('flash_message_success') !!}</strong>
  </div>
  @endif

<!-- Navigation -->
  
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="col-lg-3 col-sm-12"></div>
      <div class="col-lg-6 col-sm-12">
        <div class="account-login">
        <div class="page-title">
          <h2>Password</h2>
        </div>
        <fieldset>
          <div class="registered-users"><strong>Change Password</strong>
            <div class="content">
              <form action="{{url('/change-password')}}" method="post">{{csrf_field()}}
              <ul class="form-list">
                <li>
                  <input type="hidden" name="old_pwd" id="old_pwd" class="input-text required-entry">
                </li>
                <li>
                  <label for="current_password">Current Password<span class="required">*</span></label>
                  <input type="password" name="current_password" id="current_password" placeholder="Old Password" class="input-text required-entry validate-password">
                </li>
                <li>
                  <label for="password">New Password <span class="required">*</span></label>
                  <input type="password" name="new_pwd" id="new_pwd" placeholder="New Password" class="input-text required-entry validate-password">
                </li>
              </ul>
              <p class="required">* Required Fields</p>
              <div class="buttons-set">
                <button id="submit" type="submit" class="button login" value="Save"><span>Save</span></button>
              </div>
            </form>
            </div>
          </div>
        </fieldset>
      </div>
      </div>
      <div class="col-lg-3 col-sm-12"></div>
    </div>
  </section>

  @endsection