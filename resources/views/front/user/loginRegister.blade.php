@extends('front.layout.master')
@section('content')

<body class="login-page">
   @if(Session::has('flash_message_error'))
  <div class="alert alert-sm alert-danger alert-block" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
      <strong>{!! session('flash_message_error') !!}</strong>
  </div>
  @endif
  @if(Session::has('flash_message_success'))
  <div class="alert alert-sm alert-success alert-block" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
      <strong>{!! session('flash_message_success') !!}</strong>
  </div>
  @endif

<!-- Navigation -->
  
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="col-lg-6 col-sm-12">
        <div class="account-login">
        <div class="page-title">
          <h2>Login Account</h2>
        </div>
        <fieldset>
          <div class="registered-users"><strong>Already a Member? Just SignIn !</strong>
      <br>
      <br>
            <div class="content">
              <form action="{{url('/user-login')}}" method="post">{{csrf_field()}}
              <ul class="form-list">
                <li>
                  <label for="email">Email Address <span class="required">*</span></label>
                  <input type="email" placeholder="Email Address" class="input-text required-entry" name="email">
                </li>
                <li>
                  <label for="pass">Password <span class="required">*</span></label>
                  <input type="password" name="password" id="password" placeholder="Password" class="input-text required-entry validate-password">
                </li>
              </ul>
              <p class="required">* Required Fields</p>
              <div class="buttons-set">
                <button type="submit" value="Login" class="button login"><span>Login</span></button>
              </div>
            </form>
          </div>
        </fieldset>
        <br>
      <br>
      <br>
      </div>
      </div>
      <div class="col-lg-6 col-sm-12">
        <div class="account-login">
        <div class="page-title">
          <h2>Create an Account</h2>
        </div>
        <fieldset>
          <div class="registered-users"><strong>New User SignUp !</strong>
            <div class="content">
              <form action="{{url('/user-register')}}" method="post">{{csrf_field()}}
              <ul class="form-list">
                <li>
                  <label for="name">Name <span class="required">*</span></label>
                  <input type="text" class="input-text required-entry" name="name" id="name" placeholder="Name">
                </li>
                <li>
                  <label for="email">Email Address <span class="required">*</span></label>
                  <input type="email" name="email" id="email" placeholder="Email Address" class="input-text required-entry">
                </li>
                <li>
                  <label for="password">Password <span class="required">*</span></label>
                  <input type="password" name="password" id="password" placeholder="Password" class="input-text required-entry validate-password">
                </li>
              </ul>
              <p class="required">* Required Fields</p>
              <div class="buttons-set">
                <button id="submit" type="submit" class="button login" value="Register"><span>Register</span></button>
              </div>
            </form>
            </div>
          </div>
        </fieldset>
      </div>
      </div>
      <br>
    </div>
  </section>

  @endsection