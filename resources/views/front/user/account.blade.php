@extends('front.layout.master')
@section('content')

<body class="login-page">  
  <!-- Navigation -->
  
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="account-login">
        <div class="page-title">
          <h2>My Account</h2>
        </div>
        <div class="my-account-page">
          <div class="row">
            <div class="col-sm-4 col-md-4 col-xs-12"> <a href="{{url('/orders')}}">
              <div class="account-box">
                <div class="service-box">
                  <div class="service-icon"> <i class="fa fa-gift"></i> </div>
                  <div class="service-desc">
                    <h4>Your Orders</h4>
                    <p>Track, return, or buy things again</p>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="col-sm-4 col-md-4 col-xs-12"> <a href="{{url('/change-password')}}">
              <div class="account-box">
                <div class="service-box">
                  <div class="service-icon"> <i class="fa fa-lock"></i> </div>
                  <div class="service-desc">
                    <h4>Change Password</h4>
                    <p>Change Password</p>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="col-sm-4 col-md-4 col-xs-12"> <a href="{{url('/change-address')}}">
              <div class="account-box">
                <div class="service-box">
                  <div class="service-icon"> <i class="fa fa-location-arrow"></i> </div>
                  <div class="service-desc">
                    <h4>Change Addresses</h4>
                    <p>Edit addresses for delivery</p>
                  </div>
                </div>
              </div>
              </a> </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @endsection