<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\banner;

class admincontroller extends Controller
{
    public function index(){
    	return view('admin.dashboard');
    }

    // **************************************
    public function add_banner(){
    	return view('admin.banner.add_banner');
    }

    public function store_banner(Request $request){

    	$file = $request->file('image');
       	$filename = 'image'. time().'.'.$request->image->extension();
        $file->move("upload/",$filename);
        // dd($file);
        // die;
        $data = new banner();
        $data->url = $request->url; 
        $data->title = $request->title;       
        $data->subtitle = $request->subtitle;
        $data->image=$filename;
        
        $data->save();
        // print_r($data);
        // exit();
        if($data){
            return redirect('view_banner')->with('message','Data successfully Added');
        }
    }

    public function view_banner(){
    	$data = banner::all();
    	return view('admin.banner.view_banner', compact('data'));
    }

    public function editbanner(Request $request, $id)
    {
      if($request->isMethod('post')){
        $data =$request->all();

        if($request->hasFile('image')){   
            $data= $request->all();
            $file = $request->file('image');
            $filename = 'image'. time().'.'.$request->image->extension();
            $file->move("upload/",$filename);                  
          }
          else{
              $filename = $data['current_image'];
          }

        banner::where(['id'=>$id])->update([

          'title'=>$data['title'],
          'subtitle'=>$data['subtitle'],
          'url' =>$data['url'],
          'image'=>$filename]);

        return redirect('/view_banner')->with('flash_message_success','banner update');
      }

      $data =banner::where(['id'=>$id])->first();
      // $levels =banner::where(['parent_id'=>0])->get();
      return view('admin.banner.edit_banner')->with(compact('data'));
    }



    public function deletebanner($id)
    {
      //Alert::success('Deleted Successfully', 'Success Message');

         banner::where(['id'=>$id])->delete();
         return redirect()->back()->with('flash_message_success','Banner Deleted');
    }

}