<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use Session;

class categorycontroller extends Controller
{
    public function addCategory(Request $request)
    {   
      if($request->isMethod('post')){
      	$data=$request->all();
      	// print_r($data);
      	// die;
      	$category = new category;
      	$category->name = $data['name'];
      	$category->parent_id = $data['parent_id'];
      	$category->category_url = $data['category_url'];
      	$category->description = $data['description'];
      	$category->save();
      	return redirect('/add_category')->with('flash_message_success','Category added');
        }
      $levels= category::where(['parent_id'=>0])->get();
    	return view('admin.category.add_category')->with(compact('levels'));;
    }

    public function viewcategory()
    {
      $data = category::get();
      return view('admin.category.view_category')->with(compact('data'));
    }

    public function editcategory(Request $request, $id)
    {
      if($request->isMethod('post')){
        $data =$request->all();
        category::where(['id'=>$id])->update([

          'name'=>$data['name'],
          'parent_id'=>$data['parent_id'],
          'description' =>$data['description'],
          'category_url'=>$data['category_url']]);

        return redirect('/view_category')->with('flash_message_success','category update');
      }



      $data =category::where(['id'=>$id])->first();
      $levels =category::where(['parent_id'=>0])->get();
      return view('admin.category.edit_category')->with(compact('levels','data'));
    }



    public function deletecategory($id)
    {
      //Alert::success('Deleted Successfully', 'Success Message');

         category::where(['id'=>$id])->delete();
         return redirect()->back()->with('flash_message_success','category Deleted');
    }

    public function delete_multiple_category(Request $request){
        $delete_id= $request->input('delete_id');
        //dd($delete_id);
        category::whereIn('id',$delete_id)->delete();
        return redirect()->back()->with('success_message','Selected Product Successfully Deleted!');
    }

    public function category_status($id)
    {
        //echo $id;
        $cat = category::where('id',$id)->get()->first();
        //print_r($cat);
        if($cat->status==1)
            $status=0;
        else
            $status=1;

        // $cat1 =category::where('id',$id)->get()->first();
        //  //print_r($cat1);
        $cat->status=$status;
       $cat->update();
    }

}
