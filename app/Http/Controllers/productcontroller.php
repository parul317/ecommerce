<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\category;
use App\product_image;
use App\cart;
use App\coupon;
use Session;
use DB;
use Auth;
use App\User;
use App\Country;
use App\delivery_address;
use App\order;
use App\orderproduct;
use Razorpay\Api\Api;


class productcontroller extends Controller
{
    public function addproduct(Request $request)
    {   
      if($request->isMethod('post')){
      	$data=$request->all();
      	// print_r($data);
      	// die;
        $file = $request->file('image');
        $filename = 'image'. time().'.'.$request->image->extension();
        $file->move("upload/",$filename);

      	$product = new product;
      	$product->cat_id = $data['cat_id'];
      	$product->product_name = $data['product_name'];
      	$product->product_code = $data['product_code'];
      	$product->size = $data['size'];
        $product->overview = $data['overview'];
      	$product->description = $data['description'];
      	$product->price = $data['price'];
      	$product->quantity = $data['quantity'];
      	$product->featured_product = $data['featured_product'];
      	$product->popular_product = $data['popular_product'];
      	$product->latest_product = $data['latest_product'];
        $product->image = $filename;
      	$product->save();
      	return redirect('/add_product')->with('flash_message_success','Product added Successfully');
        }
      $levels= category::where(['parent_id'=>0])->get();
    	return view('admin.product.add_product')->with(compact('levels'));;
    }

    public function viewproduct()
    {
      $data = product::get();
      return view('admin.product.view_product')->with(compact('data'));
    }

    // public function editproduct(Request $request, $id=null)
    // {
    //   if($request->isMethod('post')){
    //     $data =$request->all();
    //     if($request->hasFile('image'))
    //     {
    //       $file = $request->file('image');
    //         $filename = 'image'. time().'.'.$request->image->extension();
    //         $file->move("upload/",$filename);

    //     product::where(['id'=>$id])->update([
    //       'cat_id' => $data['cat_id'],
    //       'product_name' => $data['product_name'],
    //       'product_code' => $data['product_code'],
    //       'size' => $data['size'],
    //       'description' => $data['description'],
    //       'price' => $data['price'],
    //       'quantity' => $data['quantity'],
    //       'featured_product' => $data['featured_product'],
    //       'popular_product' => $data['popular_product'],
    //       'latest_product' => $data['latest_product'],
    //       'image' => $filename]);

    //     return redirect('/view_product')->with('flash_message_success','category update');
    //     }
    //     else {
    //       product::where(['id'=>$id])->update([
    //       'cat_id' => $data['cat_id'],
    //       'product_name' => $data['product_name'],
    //       'product_code' => $data['product_code'],
    //       'size' => $data['size'],
    //       'description' => $data['description'],
    //       'price' => $data['price'],
    //       'quantity' => $data['quantity'],
    //       'featured_product' => $data['featured_product'],
    //       'popular_product' => $data['popular_product'],
    //       'latest_product' => $data['latest_product']]);
    //     }
    //   }

    //   $data =product::where(['id'=>$id])->first();
    //   $levels =category::where(['parent_id'=>0])->get();
    //   return view('admin.product.edit_product')->with(compact('levels','data'));
    // }

    
    public function editproduct(Request $request, $id = null){

        if($request->isMethod('post')){

          $data= $request->all();
            //echo "<pre>"; print_r($data); die;
          if($request->hasFile('image')){   
            $data= $request->all();
            //echo "<pre>"; print_r($data); die;
            $file = $request->file('image');
            // dd($file);
            // exit;
            $filename = 'image'. time().'.'.$request->image->extension();
            // dd($filename);
            // exit;
            $file->move("upload/",$filename);  
                
          }
          else{
              $filename = $data['current_image'];
          }
          product::where(['id'=>$id])->update(['cat_id'=>$data['cat_id'],'product_code'=>$data['product_code'],'product_name'=>$data['product_name'],'price'=>$data['price'],'quantity'=>$data['quantity'], 'overview'=>$data['overview'] ,'description'=>$data['description'],'featured_product'=>$data['featured_product'],'popular_product'=>$data['popular_product'],'latest_product'=>$data['latest_product'],'image'=>$filename]);
          return redirect('/view_product')->with('success_message','Product has been update successfully!');
        }

        $data= product::where(['id'=>$id])->first();
       // print_r($productDetails);
        //categories drop down start
        $levels=category::where(['parent_id'=>0])->get();
       // print_r($categories);
        $categories_dropdown="<option selected disabled>Select</option>";

        foreach ($levels as $cat) {
            if($cat->id==$data->cat_id){ 
                $selected="selected"; 

            }else{
                $selected="";
            }
            $categories_dropdown .= "<option value='".$cat->id."'".$selected.">".$cat->name."</option>";
          }  
           
        //categories dropdown end
        return view('admin.product.edit_product',compact('data','categories_dropdown'));
    }


    public function deleteproduct($id)
    {
      //Alert::success('Deleted Successfully', 'Success Message');
        product::where(['id'=>$id])->delete();
        return redirect()->back()->with('flash_message_success','Product Deleted');
    }

    public function delete_multiple_product(Request $request){
        $delete_id= $request->input('delete_id');
        //dd($delete_id);
        product::whereIn('id',$delete_id)->delete();
        return redirect()->back()->with('success_message','Selected Product Successfully Deleted!');
    }


    // public function add_images(Request $request, $id=null){
    //     if($request->isMethod('post')){

    //         $data= $request->all();
    //         if($request->hasFile('image')){

    //                 $files= $request->file('image');
    //                 //echo "<pre>"; print_r($files); die;
    //                 foreach($files as $file){

                        
    //                 $extension= $file->getClientOriginalExtension(); 
    //                 $filename=rand(1,999999).'.'.$extension;
    //                 //$large_image_path='image/backend/products/large/'.$filename;
    //                 $medium_image_path='upload/product/'.$filename;
    //                 //resize images
    //                 // Image::make($file)->save($large_image_path);
    //                  //Image::make($file)->resize(600,600)->save($medium_image_path);
    //                  //Image::make($file)->resize(300,300)->save($small_image_path);

    //                 $image= new product_image;
    //                 $image->image=$filename;
    //                 $image->product_id=$data['product_id'];
    //                 $image->save();
    //                  }

    //             }
    //             return redirect()->back()->with('message','Images Added Successfully!');
    // }
    //     $product= product::where('id',$id)->first();
    //     $images= product_image::where('product_id',$id)->get();
    //       return view('admin.product.add_image',compact('product', 'images'));
    // }


    public function add_images(Request $request, $id = null){
        if($request->isMethod('post')){

          $data= $request->all();
          if($request->hasFile('image')){
            $file = $request->file('image');
            $filename = 'image'. time().'.'.$request->image->extension();
            $file->move("upload/product/", $filename);

            $image = new product_image;
            $image->image = $filename;
            $image->product_id = $data['product_id'];
            $image->save();
            return redirect()->back()->with('message','Product Image Successfully Added!');
          }
        }

      $product = product::where('id',$id)->first();
      $images= product_image::where('product_id',$id)->get();
      return view('admin.product.add_image',compact('product', 'images'));

    }

    public function delete_image($id){
        product_image::where(['id'=>$id])->delete();
        return redirect()->back()->with('flash_message_success','Image Deleted!');
    }

    public function delete_multiple_image(Request $request){
        $delete_id= $request->input('delete_id');
        //dd($delete_id);
        product_image::whereIn('id',$delete_id)->delete();
        return redirect()->back()->with('flash_message_success','Selected Images Successfully Deleted!');
    }


    public function product_status($id)
    {
        //echo $id;
        $product = product::where('id',$id)->get()->first();
        //print_r($product);
        if($product->status==1)
            $status=0;
        else
            $status=1;
          
        $product->status=$status;
        $product->update();
    }


    public function addtoCart(Request $request){
        $data = $request->all();
        // echo "<pre>";
        // print_r($data);
        // die;

        if(empty(Auth::user()->email)){
            $data['useremail']='';
        }
        else{
            $data['useremail'] = Auth::user()->email;
        }


        $session_id = Session::getId();

        // $session_id=Session::get('session_id');
        // if(empty($session_id)){

        //    $session_id=str_random(40);
        //    Session::put('session_id',$session_id); 
        // }

        //composer require laravel/helpers
        
        //echo $session_id = str_random(40);

        // if(empty($data['session_id'])){
        //     $data['session_id']='';
        // }

        DB::table('carts')->insert(['product_id'=>$data['product_id'], 'product_name'=> $data['product_name'], 'product_code'=>$data['product_code'], 'price'=>$data['price'], 'quantity'=>$data['quantity'], 'useremail'=>$data['useremail'], 'session_id'=>$session_id]);
        //die;

        return redirect('/cart')->with('success_message','Product Succesfully Added To In Cart!');
     }


     public function cart(Request $request){
        
        if(Auth::check()){
            $user_email = Auth::user()->email;
            $userCart = DB::table('carts')->where(['useremail'=>$user_email])->get();
        }else{
            $session_id = Session::getId();
            $userCart = DB::table('carts')->where(['session_id'=>$session_id])->get();
        }
        // $session_id=Session::get('session_id'); 
        // $userCart = DB::table('carts')->where(['session_id'=>$session_id])->get();
        // print_r($userCart);
        // die();

        foreach($userCart as $key=>$products){
            //echo$products->product_id;


        $productDetail=product::where(['id'=>$products->product_id])->first();
             //$userCart[$key]->image="";
             $userCart[$key]->image=$productDetail->image;
        }
        // echo "<pre>";
        // print_r($userCart);
        // die();
       
        return view('front.cart' , compact('userCart'));
    }

    public function deleCart($id){
        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        DB::table('carts')->where('id',$id)->delete();

        return redirect()->back()->with('message','Product Succesfully Deleted!');
    }

    public function updatecartquantity($id=null,$quantity=null)
    {   
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        
        DB::table('carts')->where('id',$id)->increment('quantity',$quantity);
        return redirect('/cart')->with('message','Product quantity has been updated');

    }

     public function applyCoupon(Request $request)
    {
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        
        if($request->isMethod('post')){
            $data =$request->all();
            //echo "<pre>";print_r($data); die();
            
            $couponCount = coupon::where('coupon_code',$data['coupon_code'])->count();
            if($couponCount == 0){
                return redirect()->back()->with('flash_message_error','Coupon code does not exist');
            }
            else
            {
                $couponDetails = Coupon::where('coupon_code',$data['coupon_code'])->first();
                
                //Coupon code status
                if( $couponDetails->status == 0){
                    return redirect()->back()->with('flash_message_error','Coupon is not active');
                }   
                
                //Check coupon expiry date
                $expiry_date = $couponDetails->expiry_date;
                $current_date = date('Y-m-d');

                if($expiry_date < $current_date){
                    return redirect()->back()->with('flash_message_error','Coupon code is expired');
                }
                
                //Coupon is ready for discount
                $session_id = Session::get('session_id');
                $userCart = DB::table('carts')->where(['session_id'=>$session_id])->get();
                $total_amount = 0;

                foreach ($userCart as $item) {
                    $total_amount = $total_amount + ($item->price*$item->quantity);
                }
                
                //Check if coupon amount is fixed or percentage
                if($couponDetails->amount_type=="Fixed"){
       
                    $couponAmount = $couponDetails->amount;
                }
                else{
                    $couponAmount = $total_amount*($couponDetails->amount/100);
                }
                
                //Add coupon code in session
                Session::put('CouponAmount',$couponAmount);
                Session::put('CouponCode',$data['coupon_code']);
                return redirect()->back()->with('flash_message_success','Coupon Code is successfully applied.You are availing discount!!');

            }
        }
    }

    public function checkout(Request $request){

        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
         $shippingDetails = delivery_address::where('user_id',$user_id)->first();
       
        $userDetails = User::find($user_id);
        $countries = Country::get();
        
        //Check if shipping address exists
        $shippingCount = delivery_address::where('user_id',$user_id)->count();
        $shippingDetails = array();

        if($shippingCount > 0){
            $shippingDetails = delivery_address::where('user_id',$user_id)->first();
        }

        if($request->isMethod('post')){
            $data= $request->all();

            // echo "<pre>"; 
            // print_r($data);
            // die;
            User::where('id', $user_id)->update(['name'=>$data['billingName'], 'address'=>$data['billingAddress'], 'city'=>$data['billingCity'], 'state' => $data['billingState'],  'country'=>$data['billingCountry'],  'pincode'=>$data['billingPincode'],  'mobile'=>$data['billingMobile']]);

            if($shippingCount > 0){
                delivery_address::where('user_id', $user_id)->update(['name'=>$data['shippingName'], 'address'=>$data['shippingAddress'], 'city'=>$data['shippingCity'], 'state' => $data['shippingState'],  'country'=>$data['shippingCountry'],  'pincode'=>$data['shippingPincode'],  'mobile'=>$data['shippingMobile']]);
            }
            else{
                $shipping = new delivery_address;
                $shipping->user_id = $user_id;
                $shipping->user_email = $user_email;
                $shipping->name = $data['shippingName'];
                $shipping->address = $data['shippingAddress'];
                $shipping->city = $data['shippingCity'];
                $shipping->state = $data['shippingState'];
                $shipping->country = $data['shippingCountry'];
                $shipping->pincode = $data['shippingPincode'];
                $shipping->mobile = $data['shippingMobile'];
                $shipping->save();
                
            }
            return redirect('/order_review');
        }

        return view('front.checkout')->with(compact('userDetails', 'countries'));
    }

    public function orderreview(){
        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $shippingDetails = delivery_address::where('user_id',$user_id)->first();
        $userDetails = User::find($user_id);
        $userCart = DB::table('carts')->where(['useremail'=> $user_email])->get();
        foreach ($userCart as $key => $product) {
            $productDetails = Product::where('id',$product->product_id)->first();
            $userCart[$key]->image = $productDetails->image;
        }

        return view('front.order_review')->with(compact('userDetails', 'shippingDetails', 'userCart'));
    }

    public function placeOrder(Request $request){

        //php artisan make:model order -m
        //php artisan make:model orderproduct -m

        if($request->isMethod('post')){
         //  $data = $request->all();  
         // echo "<pre>";print_r($data);die;
          $user_id =Auth::user()->id;
          $user_email=Auth::user()->email;
          $data = $request->all();

          //Get shipping Details of Users
          $shippingDetails =delivery_address::where(['user_email'=>$user_email])->first();
          //echo "<pre>";print($shippingDetails);die;
          if(empty(Session::get('CouponCode'))){
                $coupon_code = 'Not Use';
            }else{
                $coupon_code = Session::get('CouponCode');
            }

            if(empty(Session::get('CouponAmount'))){
                $coupon_amount = '0';
            }else{
                $coupon_amount = Session::get('CouponAmount');
            }

           $order = new Order;
            $order->user_id = $user_id;
            $order->user_email = $user_email;
            $order->name = $shippingDetails->name;
            $order->address = $shippingDetails->address;
            $order->city = $shippingDetails->city;
            $order->state = $shippingDetails->state;
            $order->pincode = $shippingDetails->pincode;
            $order->country = $shippingDetails->country;
            $order->mobile = $shippingDetails->mobile;
            $order->coupone_code = $coupon_code;
            $order->coupone_amount = $coupon_amount;
            $order->order_status = "pending";
            $order->transaction_id ='';
            $order->payment_method = $data['payment_method'];
             $order->payment_status = 'pending';
            $order->grand_total = $data['grand_total'];
            $order->save();

            $order_id = DB::getPdo()->lastinsertID();
            print_r($order_id);
            die();

            $catProducts = DB::table('carts')->where(['useremail'=>$user_email])->get();
            foreach($catProducts as $pro){

                $cartPro = new OrderProduct;
                $cartPro->order_id = $order_id;
                $cartPro->user_id = $user_id;
                $cartPro->product_id = $pro->product_id;
                $cartPro->product_code = $pro->product_code;
                $cartPro->product_name = $pro->product_name;
               
                $cartPro->product_price = $pro->price;
                $cartPro->product_qty = $pro->quantity;
                $cartPro->save();
            }

            if($data['payment_method']=="cod"){
                
                    return redirect('/thanks');
                }
            if($data['payment_method']=="instamojo"){

                    return redirect('/confirm_order');
                }
            if($data['payment_method']=="paytm"){

            $amount=$data['grand_total'];


            $data_for_request = $this->handlePaytmRequest( $order_id, $amount );


            $paytm_txn_url = 'https://securegw-stage.paytm.in/theia/processTransaction';
            $paramList = $data_for_request['paramList'];
            $checkSum = $data_for_request['checkSum'];

            return view( 'paytm-merchant-form', compact( 'paytm_txn_url', 'paramList', 'checkSum' ) );
            }

            // if($data['payment_method']=="Razorpay"){
            //     $amount=$data['grand_total'];

            //     $api = new Api('rzp_test_qAN8TZjVXcCBz2', 'uiLEVHaq0E4YZCruBXBy9LMP');
            //     $order  = $api->order->create(array('receipt' => '123', 'amount' => $amount * 100 , 'currency' => 'INR'));
            //     $orderId = $order['id'];
            //     Session::put('order_id', $orderId);
            //     Session::put('amount' , $amount);

            //     return redirect()->back();
            // }
    
        } 
    }
    

    // public function pay(Request $request){
    //     $data = $request->all();
    //     // print_r($data);
    //     // die();
        
    //     $user = order::where('order_id', $data['razorpay_order_id'])->first();
    //     $user->payment_status = 'complete';
    //     $user->transaction_id = $data['razorpay_payment_id'];
    //     $user->save();
    //     return redirect('/success');

       
    // }

    ///Paytm code ////////////////////////////////////////////////
   public function handlePaytmRequest( $order_id, $amount ) {
        // Load all functions of encdec_paytm.php and config-paytm.php
        $this->getAllEncdecFunc();
        $this->getConfigPaytmSettings();

        $checkSum = "";
        $paramList = array();

        // Create an array having all required parameters for creating checksum.
       $paramList["MID"] = 'ftuWAH06700480522214';
        $paramList["ORDER_ID"] = $order_id;
        $paramList["CUST_ID"] = $order_id;
        $paramList["INDUSTRY_TYPE_ID"] = 'Retail';
        $paramList["CHANNEL_ID"] = 'WEB';
        $paramList["TXN_AMOUNT"] = $amount;
        $paramList["WEBSITE"] = 'WEBSTAGING';
        $paramList["CALLBACK_URL"] = url( '/paytm-callback' );
        $paytm_merchant_key = 'G%IR#@7N_L1sfdMA';

        //Here checksum string will return by getChecksumFromArray() function.
        $checkSum = getChecksumFromArray( $paramList, $paytm_merchant_key );

        return array(
            'checkSum' => $checkSum,
            'paramList' => $paramList
        );
    }



    /**
     * Get all the functions from encdec_paytm.php
     */
    function getAllEncdecFunc() {
        function encrypt_e($input, $ky) {
            $key   = html_entity_decode($ky);
            $iv = "@@@@&&&&####$$$$";
            $data = openssl_encrypt ( $input , "AES-128-CBC" , $key, 0, $iv );
            return $data;
        }

        function decrypt_e($crypt, $ky) {
            $key   = html_entity_decode($ky);
            $iv = "@@@@&&&&####$$$$";
            $data = openssl_decrypt ( $crypt , "AES-128-CBC" , $key, 0, $iv );
            return $data;
        }

        function pkcs5_pad_e($text, $blocksize) {
            $pad = $blocksize - (strlen($text) % $blocksize);
            return $text . str_repeat(chr($pad), $pad);
        }

        function pkcs5_unpad_e($text) {
            $pad = ord($text{strlen($text) - 1});
            if ($pad > strlen($text))
                return false;
            return substr($text, 0, -1 * $pad);
        }

        function generateSalt_e($length) {
            $random = "";
            srand((double) microtime() * 1000000);

            $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
            $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
            $data .= "0FGH45OP89";

            for ($i = 0; $i < $length; $i++) {
                $random .= substr($data, (rand() % (strlen($data))), 1);
            }

            return $random;
        }

        function checkString_e($value) {
            if ($value == 'null')
                $value = '';
            return $value;
        }

        function getChecksumFromArray($arrayList, $key, $sort=1) {
            if ($sort != 0) {
                ksort($arrayList);
            }
            $str = getArray2Str($arrayList);
            $salt = generateSalt_e(4);
            $finalString = $str . "|" . $salt;
            $hash = hash("sha256", $finalString);
            $hashString = $hash . $salt;
            $checksum = encrypt_e($hashString, $key);
            return $checksum;
        }
        function getChecksumFromString($str, $key) {

            $salt = generateSalt_e(4);
            $finalString = $str . "|" . $salt;
            $hash = hash("sha256", $finalString);
            $hashString = $hash . $salt;
            $checksum = encrypt_e($hashString, $key);
            return $checksum;
        }

        function verifychecksum_e($arrayList, $key, $checksumvalue) {
            $arrayList = removeCheckSumParam($arrayList);
            ksort($arrayList);
            $str = getArray2StrForVerify($arrayList);
            $paytm_hash = decrypt_e($checksumvalue, $key);
            $salt = substr($paytm_hash, -4);

            $finalString = $str . "|" . $salt;

            $website_hash = hash("sha256", $finalString);
            $website_hash .= $salt;

            $validFlag = "FALSE";
            if ($website_hash == $paytm_hash) {
                $validFlag = "TRUE";
            } else {
                $validFlag = "FALSE";
            }
            return $validFlag;
        }

        function verifychecksum_eFromStr($str, $key, $checksumvalue) {
            $paytm_hash = decrypt_e($checksumvalue, $key);
            $salt = substr($paytm_hash, -4);

            $finalString = $str . "|" . $salt;

            $website_hash = hash("sha256", $finalString);
            $website_hash .= $salt;

            $validFlag = "FALSE";
            if ($website_hash == $paytm_hash) {
                $validFlag = "TRUE";
            } else {
                $validFlag = "FALSE";
            }
            return $validFlag;
        }

        function getArray2Str($arrayList) {
            $findme   = 'REFUND';
            $findmepipe = '|';
            $paramStr = "";
            $flag = 1;
            foreach ($arrayList as $key => $value) {
                $pos = strpos($value, $findme);
                $pospipe = strpos($value, $findmepipe);
                if ($pos !== false || $pospipe !== false)
                {
                    continue;
                }

                if ($flag) {
                    $paramStr .= checkString_e($value);
                    $flag = 0;
                } else {
                    $paramStr .= "|" . checkString_e($value);
                }
            }
            return $paramStr;
        }

        function getArray2StrForVerify($arrayList) {
            $paramStr = "";
            $flag = 1;
            foreach ($arrayList as $key => $value) {
                if ($flag) {
                    $paramStr .= checkString_e($value);
                    $flag = 0;
                } else {
                    $paramStr .= "|" . checkString_e($value);
                }
            }
            return $paramStr;
        }

        function redirect2PG($paramList, $key) {
            $hashString = getchecksumFromArray($paramList, $key);
            $checksum = encrypt_e($hashString, $key);
        }

        function removeCheckSumParam($arrayList) {
            if (isset($arrayList["CHECKSUMHASH"])) {
                unset($arrayList["CHECKSUMHASH"]);
            }
            return $arrayList;
        }

        function getTxnStatus($requestParamList) {
            return callAPI(PAYTM_STATUS_QUERY_URL, $requestParamList);
        }

        function getTxnStatusNew($requestParamList) {
            return callNewAPI(PAYTM_STATUS_QUERY_NEW_URL, $requestParamList);
        }

        function initiateTxnRefund($requestParamList) {
            $CHECKSUM = getRefundChecksumFromArray($requestParamList,PAYTM_MERCHANT_KEY,0);
            $requestParamList["CHECKSUM"] = $CHECKSUM;
            return callAPI(PAYTM_REFUND_URL, $requestParamList);
        }

        function callAPI($apiURL, $requestParamList) {
            $jsonResponse = "";
            $responseParamList = array();
            $JsonData =json_encode($requestParamList);
            $postData = 'JsonData='.urlencode($JsonData);
            $ch = curl_init($apiURL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postData))
            );
            $jsonResponse = curl_exec($ch);
            $responseParamList = json_decode($jsonResponse,true);
            return $responseParamList;
        }

        function callNewAPI($apiURL, $requestParamList) {
            $jsonResponse = "";
            $responseParamList = array();
            $JsonData =json_encode($requestParamList);
            $postData = 'JsonData='.urlencode($JsonData);
            $ch = curl_init($apiURL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postData))
            );
            $jsonResponse = curl_exec($ch);
            $responseParamList = json_decode($jsonResponse,true);
            return $responseParamList;
        }
        function getRefundChecksumFromArray($arrayList, $key, $sort=1) {
            if ($sort != 0) {
                ksort($arrayList);
            }
            $str = getRefundArray2Str($arrayList);
            $salt = generateSalt_e(4);
            $finalString = $str . "|" . $salt;
            $hash = hash("sha256", $finalString);
            $hashString = $hash . $salt;
            $checksum = encrypt_e($hashString, $key);
            return $checksum;
        }
        function getRefundArray2Str($arrayList) {
            $findmepipe = '|';
            $paramStr = "";
            $flag = 1;
            foreach ($arrayList as $key => $value) {
                $pospipe = strpos($value, $findmepipe);
                if ($pospipe !== false)
                {
                    continue;
                }

                if ($flag) {
                    $paramStr .= checkString_e($value);
                    $flag = 0;
                } else {
                    $paramStr .= "|" . checkString_e($value);
                }
            }
            return $paramStr;
        }
        function callRefundAPI($refundApiURL, $requestParamList) {
            $jsonResponse = "";
            $responseParamList = array();
            $JsonData =json_encode($requestParamList);
            $postData = 'JsonData='.urlencode($JsonData);
            $ch = curl_init($apiURL);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $refundApiURL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $jsonResponse = curl_exec($ch);
            $responseParamList = json_decode($jsonResponse,true);
            return $responseParamList;
        }
    }

    /**
     * Config Paytm Settings from config_paytm.php file of paytm kit
     */
    function getConfigPaytmSettings() {
        define('PAYTM_ENVIRONMENT', 'TEST'); // PROD
         define('PAYTM_MERCHANT_KEY', 'G%IR#@7N_L1sfdMA'); //Change this constant's value with Merchant key downloaded from portal
        define('PAYTM_MERCHANT_MID', 'ftuWAH06700480522214'); //Change this constant's value with MID (Merchant ID) received from Paytm
        define('PAYTM_MERCHANT_WEBSITE', 'WEBSTAGING'); //Change this constant's value with Website name received from Paytm

        $PAYTM_STATUS_QUERY_NEW_URL='https://securegw-stage.paytm.in/merchant-status/getTxnStatus';
        $PAYTM_TXN_URL='https://securegw-stage.paytm.in/theia/processTransaction';
        if (PAYTM_ENVIRONMENT == 'PROD') {
            $PAYTM_STATUS_QUERY_NEW_URL='https://securegw.paytm.in/merchant-status/getTxnStatus';
            $PAYTM_TXN_URL='https://securegw.paytm.in/theia/processTransaction';
        }
        define('PAYTM_REFUND_URL', '');
        define('PAYTM_STATUS_QUERY_URL', $PAYTM_STATUS_QUERY_NEW_URL);
        define('PAYTM_STATUS_QUERY_NEW_URL', $PAYTM_STATUS_QUERY_NEW_URL);
        define('PAYTM_TXN_URL', $PAYTM_TXN_URL);
    }

    public function paytmCallback( Request $request ) {
    //return $request;
        $order_id = $request['ORDERID'];

        if ( 'TXN_SUCCESS' === $request['STATUS'] ) {
            $transaction_id = $request['TXNID'];
            $order = Order::where( 'id', $order_id )->first();
            $order->payment_status = 'complete';
            $order->transaction_id = $transaction_id;
            $order->save();
           
           $user_email = Auth::user()->email;
           DB::table('carts')->where('useremail',$user_email)->delete();
            return view( 'order-complete', compact( 'order') );
           

        } else if( 'TXN_FAILURE' === $request['STATUS'] ){
            return view( 'payment-failed' );
        }
    }


///////////////////////////////////////////////////////////////////////////////////////////////
    


    public function thanks(){
        
        $user_email = Auth::user()->email;
        DB::table('carts')->where('useremail',$user_email)->delete();
        return view('front.thanks');
    }

     Public function userorders(){
     $user_id =Auth::user()->id;
     $orders=Order::with('order')->where('user_id',$user_id)->orderBy('id','DESC')->get();
     //echo "<pre>"; print_r($orders);die;
     return view('front.userorders')->with(compact('orders'));
    }

    public function userOrderDetails($order_id){
        //$orderDetails = order::with('order')->where('id',$order_id)->first();
        $orderDetails = orderproduct::where('order_id', $order_id)->get();
        //echo "<pre>";
        //print_r($orderDetails);die;
        // $user_id = $orderDetails->user_id;
        // $userDetails = User::where('id',$user_id)->first();
        return view('front.user_order_details')->with(compact('orderDetails'));

    } 

}
