<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\banner;
use App\category;
use App\product;
use App\product_image;

class frontcontroller extends Controller
{
    public function index(){
    	$data = banner::all();
    	return view('front.index', compact('data'));
    }

    public function product_details($id){
    	$product = product::find($id);
    	$images = product_image::where('product_id',$id)->get();
        // $cat = category::where('id',$id)->get();
    	$relatedprod = product::all();
    	return view('front.product_details', compact('product', 'images', 'relatedprod'));
    }
}
