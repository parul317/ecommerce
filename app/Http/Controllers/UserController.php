<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Illuminate\Support\Facades\Hash;
use App\Country;
use App\cart;


class UserController extends Controller
{
    public function userLoginRegister(){
    	
    	return view('front.user.loginRegister');
    }

    public function register(Request $request){
    	if($request->isMethod('post')){
	      	$data=$request->all();
	      	// print_r($data);
	      	// die;
	      	$usercount = User::where('email', $data['email'])->count();
	      	if($usercount>0){
	      		return redirect()->back()->with('flash_message_error', 'Email is already exist!!');
	      	}
	      	else{
	      		$user = new user;
		      	$user->name = $data['name'];
		      	$user->email = $data['email'];
		      	$user->password = bcrypt($data['password']);
		      	$user->save();
		      	if(Auth::attempt(['email'=>$data['email'], 'password'=>$data['password']])){
		      		Session::put('frontSession',$data['email']);
		      		return redirect('/cart');
		      	}
	      	}
    	}
	}


	public function logout(){
        //Session::forget('frontSession');
    	Auth::logout();
    	return redirect('/');
    }

    public function login(Request $request){
        $session = Session::getId();
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		// echo "<pre>";print_r($data);die;
    		if (Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
                //Session::put('frontSession',$data['email']);
                cart::where('session_id',$session)->update([
               'useremail'=>$data['email']]);
    			return redirect('/cart');
    		}else{
    			return redirect()->back()->with('flash_message_error','Invalid username or password');
    		}
    	}
    }

    public function account(){

        //php artisan make:middleware frontlogin
        //kernel.php
        //'frontlogin'=> \App\Http\Middleware\frontlogin::class,

        return view('front.user.account');
    }

    public function changePassword(Request $request){

        if($request->isMethod('post')){
            $data = $request->all();
            $old_pwd = User::where('id',Auth::User()->id)->first();
            $current_password = $data['current_password'];
            if(Hash::check($current_password,$old_pwd->password)){
                $new_pwd = bcrypt($data['new_pwd']);
                User::where('id',Auth::User()->id)->update(['password'=>$new_pwd]);
                return redirect()->back()->with('flash_message_success','Your Password is changed now!!');
            }else{
                return redirect()->back()->with('flash_message_error','Old Password is incorrect!!');
            }
        }
        
        return view('front.user.changePassword');
    }

    public function changeAddress(Request $request){
    	   
        // https://github.com/raramuridesign/mysql-country-list/blob/master/country-lists/mysql-country-list.sql
        $user_id = Auth::user()->id;
        $UserDetails = User::find($user_id);

    	if($request->isMethod('post')){
			$data= $request->all();

            $user = User::find($user_id);
            $user->name = $data['name'];
            $user->city = $data['city'];
            $user->address = $data['address'];
            $user->state = $data['state'];
            $user->country = $data['country'];
            $user->pincode = $data['pincode'];
            $user->mobile = $data['mobile'];
            $user->save();
            return redirect()->back()->with('flash_message_success', 'Account Details has been Updated!!');
		}

        $country = Country::all();
        return view('front.user.address', compact('country', 'UserDetails'));
    }
}
