<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cart;
use Session;
use DB;
use Auth;
use App\User;
use App\order;
use App\orderproduct;
use App\delivery_address;

class orderController extends Controller
{
    // public function viewOrder(){
    // 	$data = order::get();

    // 	return view('admin.order.view_orders')->with(compact('data'));
    // }

    public function viewOrder()
    {
        
       $data =order::with('order')->orderBy('id','DESC')->get();
       return view('admin.order.view_orders')->with(compact('data'));
    }

    public function viewOrderDetail($order_id){
        
        $orderDetails = order::with('order')->where('id',$order_id)->first();
        $user_id = $orderDetails->user_id;
        $userDetails = User::where('id',$user_id)->first();
        // print_r($user_id);
        // die();
        return view('admin.order.order_details')->with(compact('orderDetails','userDetails'));
    }
 	public function updateOrderStatus(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
        }
        Order::where('id',$data['order_id'])->update(['order_status'=>$data['order_status']]);
        return redirect()->back()->with('flash_message_success','Order status has been updated successfully!');
    }

    
}
