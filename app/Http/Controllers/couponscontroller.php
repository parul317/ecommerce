<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\coupon;

class couponscontroller extends Controller
{
    public function addcoupon(Request $request){
    	if($request->isMethod('post')){
      	$data=$request->all();
   
      	$coupon = new coupon;
      	$coupon->coupon_code = $data['coupon_code'];
      	$coupon->amount = $data['amount'];
      	$coupon->amount_type = $data['amount_type'];
      	$coupon->expiry_date = $data['expiry_date'];
      	$coupon->save();
      	return redirect('/view_coupon')->with('flash_message_success','coupon added Successfully');
        }
      
    	return view('admin.coupon.add_coupon');
    }

    public function viewcoupon()
    {
      $data = coupon::get();
      return view('admin.coupon.view_coupon')->with(compact('data'));
    }

    public function editcoupon(Request $request, $id=null)
    {
      if($request->isMethod('post')){
        $data =$request->all();
        coupon::where(['id'=>$id])->update([

          'coupon_code'=>$data['coupon_code'],
          'amount'=>$data['amount'],
          'amount_type' =>$data['amount_type'],
          'expiry_date'=>$data['expiry_date']]);

        return redirect('/view_coupon')->with('flash_message_success','coupon update');
      }

      $data =coupon::where(['id'=>$id])->first();      
      return view('admin.coupon.edit_coupon')->with(compact('data'));
    }

    public function deletecoupon($id)
    {
      //Alert::success('Deleted Successfully', 'Success Message');

         coupon::where(['id'=>$id])->delete();
         return redirect()->back()->with('flash_message_success','Coupon Deleted');
    }

    public function delete_multiple_coupon(Request $request){
        $delete_id= $request->input('delete_id');
        //dd($delete_id);
        coupon::whereIn('id',$delete_id)->delete();
        return redirect()->back()->with('success_message','Selected Coupon Successfully Deleted!');
    }

    public function coupon_status($id)
    {
        $coupon = coupon::where('id',$id)->get()->first();
        //print_r($coupon);
        if($coupon->status==1)
            $status=0;
        else
            $status=1;

        $coupon->status=$status;
       	$coupon->update();
    }

}
