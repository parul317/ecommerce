<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    //has many relation
    public function order(){
    	return $this->hasMany('App\OrderProduct','order_id');
    }
}
